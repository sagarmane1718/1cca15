package Programming;

public class Program3 {
    public static void main(String[] args) {
        int rows=5;
        int columns=5;

        for(int i=0;i<rows;i++){
            for(int j=0;j<columns;j++){
                if(i==0||i==4||j==0||j==4||i==j){
                    System.out.print(" * ");
                }else{
                    System.out.print("   ");
                }
            }
            System.out.println();
        }

    }
}
