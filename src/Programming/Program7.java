package Programming;

public class Program7 {
    public static void main(String[] args) {
        int line=5;
        int star=1;

        for(int i=0;i<line;i++){
            for(int j=0;j<star;j++){
                if(j==0||i==4||i==j){
                    System.out.print(" * ");
                }else{
                    System.out.print("   ");
                }
            }
            System.out.println();
            star++;
        }
    }
}
