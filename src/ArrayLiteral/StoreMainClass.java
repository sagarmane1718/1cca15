package ArrayLiteral;

import java.util.Scanner;

public class StoreMainClass {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean status=true;
        while(status){
            System.out.println("Select Product");
            System.out.println("0:TV\n1:Projector\n2:Mobile\n3:exit");
            int choice= sc1.nextInt();
            System.out.println("Enter Quantity");
            int qty=sc1.nextInt();

            //Object Creation
            StoreManager s1=new StoreManager();
            if(choice==0||choice==1||choice==2){
                s1.calculateBill(choice,qty);
            }else{
                System.out.println("Invalid Choice");
                status=false;


            }
        }
    }
}
