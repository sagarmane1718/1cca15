package ArrayLiteral;

import java.util.Scanner;

public class ArrayDemo1 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        BillCalculator b1=new BillCalculator();
        System.out.println("Enter Total No OF Bills");
        int count=sc1.nextInt();
        double[] amounts=new double[count];
        System.out.println("Enter Bill Amounts");

        //Accept Bill Amount From users
        for(int a=0;a<count;a++){
            amounts[a]=sc1.nextDouble();
        }
        //Call Business Logic class
        b1.calculateBill(amounts);
    }
}
