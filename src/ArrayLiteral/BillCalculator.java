package ArrayLiteral;

public class BillCalculator {
    public void calculateBill(double[] amounts){

        //Store gst Values
        double[] gstValues=gstCalculation(amounts);

        //Create new array to calculate total Amount
        double[] totalAmounts=new double[amounts.length];

        //Perform sum of Amounts And gst Values
        for(int a=0;a< amounts.length;a++){
            totalAmounts[a]=amounts[a]+gstValues[a];
        }
        double totalBillamounts=0.0;
        double totalGstAmounts=0.0;
        double totalFinalAmount=0.0;

        //Perform Sum of Amounts And gst Values
        for(int a=0;a< amounts.length;a++){
            totalBillamounts+=amounts[a];
            totalGstAmounts+=gstValues[a];
            totalFinalAmount+=totalAmounts[a];
        }
        //Present Final Output to User
        System.out.println("BillAmt\tGstAmt\tTotal");
        System.out.println("==========================");
        for(int a=0;a< amounts.length;a++){
            System.out.println(amounts[a]+"\t"+gstValues[a]+"\t"+totalAmounts[a]);
        }
        System.out.println("==========================");
        System.out.println(totalBillamounts+"\t"+totalGstAmounts+"\t"+totalFinalAmount);
    }
    public double [] gstCalculation(double[] amounts){

        //Create New array to Store GstAmt
        double[] gstAmounts=new double[amounts.length];
        for(int a=0;a< amounts.length;a++){
            if(amounts[a]<500){
                gstAmounts[a]=amounts[a]*0.05;//5% gst
            }else{
                gstAmounts[a]=amounts[a]*0.1;//10% gst
            }
        }
        //Return gst Array to Calculate Bill Method
        return gstAmounts;
    }
}
