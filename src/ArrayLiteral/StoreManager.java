package ArrayLiteral;

//Business Logic class
public class StoreManager {
    static String[] products={"TV","Projector","Mobile"};
    static double[] price={12000,15000,20000};
    static int[] stock={25,10,50};

    void calculateBill(int choice,int qty){
        boolean found=false;
        for(int a=0;a<products.length;a++){
            if(choice==a && qty<=stock[a]){
                double total=qty*price[a];
                stock[a]-=qty;
                System.out.println("Total Bill amount:"+total);
                found=true;
            }
        }
        if(! found){
            System.out.println("Product Not found/Out of Stock");
        }
    }
}
