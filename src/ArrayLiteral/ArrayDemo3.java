package ArrayLiteral;

public class ArrayDemo3 {
    public static void main(String[] args) {
        int[] data=new int [3];
        data[0]=100;
        data[2]=200;
        data[3]=300;
        printArray(data);
    }
    static void printArray(int[] info){
        for(int a=0;a< info.length;a++){
            System.out.println(info[a]);
        }
    }
}
