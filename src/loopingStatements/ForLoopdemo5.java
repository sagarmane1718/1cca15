package loopingStatements;

import java.util.Scanner;

public class ForLoopdemo5 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Start Point");
        int start= sc1.nextInt();
        System.out.println("enter End Point");
        int end= sc1.nextInt();
        double sum=0.0;
        for (int a=start;a<=end;a++){
            if (a%2==0){
                sum+=a;
            }
        }
        System.out.println("sum:"+sum);
    }
}
