package Array;

public class ArrayDemo2 {
    public static void main(String[] args) {
        double[] arr1=new double[3];
        arr1[0]=35.45;
        arr1[1]=22.56;
        //arr1[3]=17.45;-->Array index is out of bounds
        for(int a=0;a<=2;a++){
            System.out.println(arr1[a]);
        }
    }
}
