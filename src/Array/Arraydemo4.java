package Array;

import java.util.Scanner;

public class Arraydemo4 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        double sum=0.0;
        System.out.println("Enter total No of Subject");
        int size=sc1.nextInt();
        System.out.println("Enter Marks");
        double[] marks=new double[size];

        for(int a=0;a<size;a++){
            marks[a]=sc1.nextDouble();
            sum+=marks[a];
        }
        System.out.println("Sum: "+sum);
        System.out.println("Percent: "+sum/size);
    }
}
