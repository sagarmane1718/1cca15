package Array;

import java.util.Scanner;

public class ArrayDemo3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Enter Total No of Courses");
        int size= sc1.nextInt();
        String[] course=new String[size];
        System.out.println("Enter course Names");
        //Accept course Names
        for(int a=0;a<size;a++){
            course[a]=sc1.next();
        }
        System.out.println("Selected Courses");
        for(int a=0;a<size;a++){
            System.out.println(course[a]);
        }
    }
}
