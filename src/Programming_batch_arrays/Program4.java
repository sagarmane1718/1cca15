/*
To storing random integers in an array
 */
package Programming_batch_arrays;

import java.util.Random;

public class Program4 {

    public static void main(String[] args) {
        Random rd = new Random(); // creating Random object
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rd.nextInt(5); // storing random integers in an array
            System.out.print(arr[i]+" "); // printing each array element
        }
    }
}
