/*
To Find 2nd Max
 */


package Programming_batch_arrays;

import java.util.Arrays;
import java.util.Random;

public class Program11 {
    public static void main(String[] args) {
        Random rd=new Random();
        int[] arr=new int[5];

        int max1=arr[0];
        int max2=arr[1];

        for(int a=0;a< arr.length;a++){
            arr[a]=rd.nextInt(5);

            if(arr[a]>max1){
                max2=max1;
                max1=arr[a];
            }
            else if (arr[a]>max2 && arr[a]!=max1){
                max2=arr[a];
            }
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("Max value : "+max1);
        System.out.println("2nd Max value :"+max2);
    }
}
