/*
Decimal to Binary

 */

package Programming_batch_arrays;

public class Program12 {
    public static void main(String[] args) {
        int a=10;
        int[] arr=new int[10];
        int i=0;

        while(a!=0){
            arr[i]=a%2;
            i++;
            a/=2;
        }

        for(int j= i;j>=0;j--)
            System.out.print(arr[j]);
    }
}
