/*
To Find Max value
 */
package Programming_batch_arrays;

public class Program9 {
    public static void main(String[] args) {
        int[]arr={10,20,30,40,50,60,70};
        int max=arr[0];

        for(int a=0;a< arr.length;a++){
            if (arr[a]>max)
                max=arr[a];
        }
        System.out.println("Max value: "+max);
    }
}
