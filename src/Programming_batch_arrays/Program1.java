/*
Printing Array
 */
package Programming_batch_arrays;

public class Program1 {
    public static void main(String[] args) {
        int[]arr={10,20,30,40,50,60};

        for(int a:arr){
            System.out.print(a+" ");
        }
    }
}
