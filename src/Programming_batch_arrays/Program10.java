/*
To Find Max Value from Random
 */


package Programming_batch_arrays;

import java.util.Arrays;
import java.util.Random;

public class Program10 {
    public static void main(String[] args) {
        Random rd=new Random();
        int[] arr=new int[5];

        int max=arr[0];

        for(int a=0;a< arr.length;a++){
            arr[a]=rd.nextInt(5);
            if (arr[a]>max)
                max=arr[a];
        }
        System.out.println(Arrays.toString(arr));
        System.out.println("Max Value: "+max);
    }
}
