/*
Checking Duplicate Value in Array//Wrong
 */
package Programming_batch_arrays;

import java.util.Random;

public class Program14 {
    public static void main(String[] args) {
        Random rd=new Random();
        int[] arr= new int[10];

        for(int a:arr)
            System.out.print(a+" ");
        System.out.println();
        for(int i=0;i< arr.length;i++){
            arr[i]=rd.nextInt(10);
            for(int j=i+1;j< arr.length;j++)
                if(arr[i]==arr[j])
                    System.out.println(arr[i]);
        }
    }
}
