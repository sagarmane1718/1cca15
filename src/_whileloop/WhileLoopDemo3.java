package _whileloop;

import java.util.Scanner;

public class WhileLoopDemo3 {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        boolean status=true;
        while(status){
            System.out.println("1:Addition");
            System.out.println("2:Subtraction");
            System.out.println("3:Exit");
            int choice=sc1.nextInt();
            System.out.println("Enter No 1");
            int no1=sc1.nextInt();
            System.out.println("Enter No 2");
            int no2=sc1.nextInt();
            if (choice == 1) {
                System.out.println(no1+no2);
            }
            else if(choice==2){
                System.out.println(no1-no2);
            }
            else{
                status=false;
            }
        }
    }
}
