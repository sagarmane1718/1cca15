package _whileloop;

import java.util.Scanner;

public class SwitchDemo {
    public static void main(String[] args) {
        Scanner sc1=new Scanner(System.in);
        System.out.println("Select Language");
        System.out.println("1:Java\n2:Python\n3:Php");
        int choice=sc1.nextInt();
        switch (choice){
            case 1:
                System.out.println("Selected LAnguage is Java");
                break;
            case 2:
                System.out.println("Selected LAnguage is Python");
                break;
            case 3:
                System.out.println("Selected LAnguage is Php");
                break;
            default:
                System.out.println("Invalid Choice");
        }
    }
}
