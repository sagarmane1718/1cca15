/*
00001
00020
00300
04000
50000
 */

package GroomingSession;

public class Program12 {
    public static void main(String[] args) {
        int line=5;
        int star=5;
        int ch=1;

        for(int i=0;i<line;i++){
            for(int j=0;j<star;j++){
                if((i+j)==4)
                System.out.print(ch);
                else{
                    System.out.print(0);
                }
            }
            System.out.println();
            ch++;
        }
    }
}
