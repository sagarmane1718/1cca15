package Class1CCA15;

public class StudentInfo {
    public static void main(String[] args) {
        //Declaration
        int rollNo;
        String name;
        float percent;
        long contact;
        String emailId;

        //Initialization
        rollNo=16;
        name="sachin";
        percent=77.15f;
        contact= 1238519625L;
        emailId="abc12345@gmail.com";

        //Print Info
        System.out.println("Name: "+name);
        System.out.println("Roll No.: "+rollNo);
        System.out.println("Percentage: "+percent);
        System.out.println("contact: "+contact);
        System.out.println("Email Id: "+emailId);
    }
}
