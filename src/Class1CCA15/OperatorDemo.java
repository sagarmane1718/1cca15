package Class1CCA15;

public class OperatorDemo {
    public static void main(String[] args) {

        //Numeric
        System.out.println(10+12);//22
        System.out.println(12.25+36.78);//49.03
        System.out.println(10+35.25);//45.25

        //Numeric+String
        System.out.println(10+"Java");//10Java
        System.out.println(10+5+"SQL");//15SQL
        System.out.println("Java"+10+20);//Java1020
        System.out.println("Java"+(10+20));//Java30

        //Char,String,Numeric,
        System.out.println('A'+'B');//131(ASCI Values)
        System.out.println('A'+22);//87
        System.out.println('C'+"Java");//CJava
        System.out.println("Core"+"Java");//CoreJava

        //Boolean,Numeric
        System.out.println("10"+true);//10true
      //  System.out.println(10+true);//Error
    }
}
