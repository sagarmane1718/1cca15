package Class1CCA15;

public class Demo2 {
    public static void main(String[] args) {
        double basicSalary=12000;
        double hra=6000;
        double pf=800;
        double incentives=3000;
        double profTax=200;

        //Gross Salary
        double grossSalary=basicSalary+hra+incentives;

        //total Deduction
        double deduction=pf+profTax;

        //Net Salary
        double netSalary=grossSalary-deduction;

        //Print Values
        System.out.println("Gross Salary: "+grossSalary);
        System.out.println("Total Deduction: "+deduction);
        System.out.println("============================");
        System.out.println("Net Salary: "+netSalary);
    }
}
