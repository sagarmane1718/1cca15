package Class1CCA15;

public class Demo3 {
    public static void main(String[] args) {
        int sub1=89;
        int sub2=39;
        int sub3=73;
        int totalmarks=250;

        //Recieved marks
        double recievedMarks=sub1+sub2+sub3;

        //calculate Percentage
        double percent=recievedMarks/totalmarks*100;

        //Print values
        System.out.println("Recieved marks: "+recievedMarks);
        System.out.println("Out of Total Marks: "+totalmarks);
        System.out.println("=========================");
        System.out.println("Percentage: "+percent);
    }
}
