package Class1CCA15;

public class Employee {
    public static void main(String[] args) {
        //Declaration
        int empId;
        String empName;
        double empSalary;

        //Initialization
        empId=123;
        empName="Dinesh";
        empSalary=27000.51;

        //Print Values
        System.out.println("Employee Id: "+empId);
        System.out.println("Employee Name: "+empName);
        System.out.println("Employee Salary: "+empSalary);
    }
}
