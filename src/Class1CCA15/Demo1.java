package Class1CCA15;

public class Demo1 {
    public static void main(String[] args) {
        int productQty=25;
        double productPrice=500.26;

        //Calculate Total Amount
        double total=productPrice*productQty;

        //discount 10%
        double discountedAmt=total*0.1;

        //Latest Amount
        double latestAmt=total-discountedAmt;

        //Print
        System.out.println("Without discount: "+total);
        System.out.println("Total Discount: "+discountedAmt);
        System.out.println("=============================");
        System.out.println("Final Amount: "+latestAmt);
    }
}
