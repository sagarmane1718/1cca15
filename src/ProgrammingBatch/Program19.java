/*
Count specific digit
 */

package ProgrammingBatch;

public class Program19 {
    public static void main(String[] args) {
        long a=111000102130l;
        int count=0;
        while(a!=0){
            long r=a%10;
            if(r==0)
                count++;
            a=a/10;
        }
        System.out.println(count);
    }
}
