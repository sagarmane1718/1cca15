package ProgrammingBatch;

public class Program20 {
    static int n1=0;
    static int n2=1;
    public static void main(String[] args) {
        int a=10;
        fabonacci(a);

    }
    public static void fabonacci(int a){
        if(a>=0){
            System.out.println(n1);
            int sum=n1+n2;
            n1=n2;
            n2=sum;
            a--;
            fabonacci(a);
        }
    }
}
